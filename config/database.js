module.exports = ({ env }) => ({
  connection: {
    client: "postgres",
    connection: {
      host: env(
        "DATABASE_HOST",
        "dpg-c8up94n9re0l61bbi1mg.frankfurt-postgres.render.com"
      ),
      port: env.int("DATABASE_PORT", 5432),
      database: env("DATABASE_NAME", "gbs_predictor_db_staging"),
      user: env("DATABASE_USERNAME", "gbs_predictor_db_staging_user"),
      password: env("DATABASE_PASSWORD", "7BxRLbdGwsegywGWMj46pwFIrBBVZj8E"),
      ssl: env.bool("DATABASE_SSL", true),
    },
  },
});
